package com.blackdog.qrbuilder.core.model;

public enum ImageTypes {
    
  NONE(null, null, null),
  GIF("image/gif", ".gif", net.glxn.qrgen.core.image.ImageType.GIF),
  JPG("image/jpg", ".jpg", net.glxn.qrgen.core.image.ImageType.JPG), 
  PNG("image/png", ".png", net.glxn.qrgen.core.image.ImageType.PNG), 
  BMP("image/bmp", ".bmp", net.glxn.qrgen.core.image.ImageType.BMP),
  WEBP("image/*", null, null), 
  ORIGINAL("", null, null);
  
  private String mime;
  private String extension;
  private net.glxn.qrgen.core.image.ImageType imageType;

  private ImageTypes(String mime, String extension, net.glxn.qrgen.core.image.ImageType imageType) {
    this.mime = mime;
    this.extension = extension;
    this.imageType = imageType;
  }

  public String getMime() {
    return mime;
  }

  public String getExtension() {
    return extension;
  }

  public net.glxn.qrgen.core.image.ImageType getImageType() {
    return imageType;
  }
  
}
