package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.exceptions.QRCodeException;
import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.EMail;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class MailCodeInfo extends QRCode {

  private EMail content;
  
  public MailCodeInfo() {
    super(QRCodeType.MAIL);
    this.content = new EMail();
  }
  
  public MailCodeInfo(String email) {
    this();
    
    setEmail(email);
  }
  
  
  public String getEmail() {
    return this.content.getEmail();
  }

  public MailCodeInfo setEmail(String email) {
    this.content.setEmail(email);
    return this;
  }
  
  public void specificValidations() throws QRCodeException {
    System.out.println("validacoes especificas do mailCode");
    
    /*if(this.address == null || this.address.equals("")) {
      throw new QRCodeException("The Address is empty.");
    }
    if(this.subject == null || this.subject.equals("")) {
      throw new QRCodeException("The Subject is empty.");
    }
    if(this.content == null || this.content.equals("")) {
      throw new QRCodeException("The Content is empty.");
    }*/
    
  }

  @Override
  public String generateString() {
    return this.content.generateString();
  }

  
  
}
