package com.blackdog.qrbuilder.core;

import org.apache.commons.codec.binary.Base64;

import com.blackdog.qrbuilder.core.dto.QRCodeDto;
import com.blackdog.qrbuilder.core.exceptions.QRCodeException;
import com.blackdog.qrbuilder.core.model.ImageTypes;
import com.blackdog.qrbuilder.core.model.OutputFormat;
import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeConfig;
import com.google.zxing.WriterException;

import net.glxn.qrgen.core.image.ImageType;


/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class QRConfigBuilder {

  private QRCodeConfig config;
  
  private QRCode code;
  
	public QRConfigBuilder() {
    super();
    this.config = new QRCodeConfig()
                      .setSize(1024)
                      .setOutputFormat(OutputFormat.JPG)
                      .setImageType(ImageTypes.JPG);
  }

	public QRConfigBuilder setCode(QRCode code) {
	  this.code = code;
	  return this;
	}
	
	public QRCodeConfig getConfig() {
	  return config;
	}
	
	public QRConfigBuilder setConfig(QRCodeConfig config) {
	  this.config = config;
	  return this;
	}
	
	
	/*
   * 
   * Public Build Methods
   * 
   */
  

  /**
   * Build a QRCode as byte[]
   * 
   * @return
   * @throws QRCodeException
   */
  public QRCodeDto build() throws QRCodeException {
    
    // Executar Build e encapsular
    
    QRCodeDto dto = new QRCodeDto();
    
    dto.setDebug(code.generateString());
    
    String data = Base64.encodeBase64String(buildQRCodeUtil());
    dto.setCodeImage(data);
    
    return dto;
  }
  
  /**
   * 
   * @return
   * @throws QRCodeException
   */
  public byte[] buildAsImage() throws QRCodeException {
    
    byte[] data = buildQRCodeUtil();
    
    return data;
  }

  /**
   * Build the QRCode as String Base64
   * 
   * @return
   */
  public String buildAsBase64() throws QRCodeException {
    
    try {
      
      byte[] data = buildQRCodeUtil();
      
      String base64Image = Base64.encodeBase64String(data);
      
      return base64Image;
    } catch (net.glxn.qrgen.core.exception.QRGenerationException e) {
      
      e.printStackTrace();
      
      if(e.getCause() != null && e.getCause() instanceof com.google.zxing.WriterException) {
       
        WriterException ze = (com.google.zxing.WriterException) e.getCause();
        
        throw new QRCodeException("QRCode Build Fail: " + ze.getMessage(), e);
        
      }
      
      throw new QRCodeException("QRCode Build Fail", e);
    }
    
  }


  /*
   * 
   * Build of QRCode
   * 
   */
  private byte[] buildQRCodeUtil() throws QRCodeException {
    
    validations();
        
    net.glxn.qrgen.javase.QRCode qrcode = net.glxn.qrgen.javase.QRCode.from(code.generateString());
    
    /*
    net.glxn.qrgen.javase.QRCode qrcode = null; 
    
    if(code.getType().equals(QRCodeType.VCARD)) {
      
      qrcode = net.glxn.qrgen.javase.QRCode.from(((VCardCodeInfo) code).getContent());
      
    } else
    if(code.getType().equals(QRCodeType.CALL)) {
        
      qrcode = net.glxn.qrgen.javase.QRCode.from(((CallCodeInfo) code).getContent());
    } else
    if(code.getType().equals(QRCodeType.CONTACT)) {
        
      qrcode = net.glxn.qrgen.javase.QRCode.from(((ContactCodeInfo) code).getContent());
    } else
    if(code.getType().equals(QRCodeType.EVENT)) {
            
      qrcode = net.glxn.qrgen.javase.QRCode.from(((EventCodeInfo) code).getContent());
     
      //TODO Fazer os outros
    } else {
      qrcode = net.glxn.qrgen.javase.QRCode.from(( String ) code.getContent());
    }*/
      
    
    qrcode.withCharset("UTF-8");
    qrcode.withSize(config.getSize(), config.getSize());
    
    //Level
    if (config.getErrorLevel() != null) {
      qrcode.withErrorCorrection(config.getErrorLevel().getLevel());
    }
    
    //Image Type
    ImageType imageType = config.getImageType().getImageType();  
    if(imageType == null) {
      qrcode.to(net.glxn.qrgen.core.image.ImageType.JPG);
    } else {
      qrcode.to(imageType);
    }
    
    //Outross
    
    return qrcode.stream().toByteArray();
  }
  
  
  private void validations() throws QRCodeException {
    
    /** Apply defaults **/
    
    config.specificValidations();
    
    code.specificValidations();
    
    /** Post Specifiques Validations **/
    //TODO 
    /*if(code.getContent() == null || code.getContent().equals("")) {
      throw new QRCodeException("The Content is null.");
    }*/
  }
	
}
