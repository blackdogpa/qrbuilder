package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.exceptions.QRCodeException;
import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.VCard;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 * VCard johnDoe = new VCard("John Doe")
                .setName("John Doe")
                .setEmail("john.doe@example.org")
                .setAddress("John Doe Street 1, 5678 Berlin")
                .setTitle("Mister")
                .setCompany("John Doe Inc.")
                .setPhoneNumber("1234")
                .setWebsite("www.example.org");
        File file = QRCode.from(johnDoe).withSize(1024, 1024).file();
        Assert.assertNotNull(file);
 *
 */
public class VCardCodeInfo extends QRCode/*<VCard>*/ {
	
	private VCard content;
	
  public VCardCodeInfo() {
    super(QRCodeType.VCARD);
    this.content = new VCard();
  }
  
  public void specificValidations() throws QRCodeException {
    
    if(this.content.getName() == null) {
      throw new QRCodeException("VCard: The Name can't be null.");
    }
    
    if(this.content.getPhoneNumber() == null) {
      throw new QRCodeException("VCard: The Phone Number can't be null.");
    }
    
  }
  
  @Override
  public String generateString() {
    return this.content.generateString();
  }
  
  
  public String getName() {
    return this.content.getName();
  }

  public VCardCodeInfo setName(String name) {
    this.content.setName(name);
    return this;
  }

  public String getCompany() {
    return this.content.getCompany();
  }

  public VCardCodeInfo setCompany(String company) {
    this.content.setCompany(company);
    return this;
  }

  public String getPhoneNumber() {
    return this.content.getPhoneNumber();
  }

  public VCardCodeInfo setPhoneNumber(String phoneNumber) {
    this.content.setPhoneNumber(phoneNumber);
    return this;
  }

  public String getTitle() {
    return this.content.getTitle();
  }

  public VCardCodeInfo setTitle(String title) {
    this.content.setTitle(title);
    return this;
  }

  public String getEmail() {
    return this.content.getEmail();
  }

  public VCardCodeInfo setEmail(String email) {
    this.content.setEmail(email);
    return this;
  }

  public String getAddress() {
    return this.content.getAddress();
  }

  public VCardCodeInfo setAddress(String address) {
    this.content.setAddress(address);
    return this;
  }

  public String getWebsite() {
    return this.content.getWebsite();
  }

  public VCardCodeInfo setWebsite(String website) {
    this.content.setWebsite(website);
    return this;
  }

  public String getNote() {
    return this.content.getNote();
  }

  public VCardCodeInfo setNote(String note) {
    this.content.setNote(note);
    return this;
  }


  @Override
  public String toString() {
    return "VCardCodeInfo [getName()=" + getName() + ", getCompany()=" + getCompany() + ", getPhoneNumber()=" + getPhoneNumber() + ", getTitle()="
            + getTitle() + ", getEmail()=" + getEmail() + ", getAddress()=" + getAddress() + ", getWebsite()=" + getWebsite() + ", getNote()="
            + getNote() + "]";
  }

}
