package com.blackdog.qrbuilder.core.model;

import com.blackdog.qrbuilder.core.exceptions.QRCodeException;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public abstract class QRCode/*<T>*/ {

  private QRCodeType type;

  public QRCode(QRCodeType type) {
    super();
    this.type = type;
  }

  public void specificValidations() throws QRCodeException {
    System.out.println("validacoes especificas do DEFALT");
  }
  
  public abstract String generateString();

  //public abstract Schema getContent();

  public QRCodeType getType() {
    return type;
  }

  /*public void setType(QRCodeType type) {
    this.type = type;
  }*/

  
  /**
   * 
   * @param size Size in pixels
   * 
   * @return
   */
/*  public QRCode setSize(Integer size) {
    this.size = size;
    return this;
  }

  public Integer getSize() {
    return size;
  }

  public OutputFormat getOutputFormat() {
    return outputFormat;
  }*/

  /**
   * 
   * @param outputFormat JPG, GIF or PNG
   * @return
   */
  /*public QRCode setOutputFormat(OutputFormat outputFormat) {
    this.outputFormat = outputFormat;

    return this;
  }

  public CryptoConfig getCryptoConfig() {
    return cryptoConfig;
  }

  public QRCode setCryptoConfig(CryptoConfig cryptoConfig) {
    this.cryptoConfig = cryptoConfig;

    return this;
  }
  
  public ErrorLevel getErrorLevel() {
    return errorLevel;
  }

  public QRCode setErrorLevel(ErrorLevel errorLevel) {
    this.errorLevel = errorLevel;
    return this;
  }

  public ImageTypes getImageType() {
    return imageType;
  }

  public QRCode setImageType(ImageTypes imageType) {
    this.imageType = imageType;
    return this;
  }
*/
}
