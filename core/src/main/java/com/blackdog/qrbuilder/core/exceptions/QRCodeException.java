package com.blackdog.qrbuilder.core.exceptions;


/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class QRCodeException extends Exception {

  private static final long serialVersionUID = -1299751693901625059L;
  
  private String id;
  private String code;
  
  public QRCodeException() {
    super();
  }
  
  public QRCodeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }



  public QRCodeException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }



  public QRCodeException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }



  public QRCodeException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }



  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
  
  
  
/*
  public QRCodeException(String message, Object... args) {
    super(StringUtils.messageFormat(message, args));
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(StringUtils.messageFormat(message, args)));
  }
  
  public QRCodeException(Throwable cause) {
    super(cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(cause.getMessage()));
  }
  
  public QRCodeException(Throwable cause, Validation validation) {
    super(cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(validation);
    
    this.code = validation.getCode();
  }
  
  public QRCodeException(Validation validation) {
    super();
    this.validations = new ArrayList<Validation>();
    this.validations.add(validation);
    
    this.code = validation.getCode();
  }

  @Deprecated//TODO Esse construtor não contem o Code
  public QRCodeException(Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(StringUtils.messageFormat(message, args)));
  }
  
  
  public QRCodeException(List<Validation> validations) {
    super();
    this.validations = validations;
    
    this.code = getCodeFromList(validations);
  }
  
  public QRCodeException(List<Validation> validations, String message,  Object... args) {
    super(StringUtils.messageFormat(message, args));
    this.validations = validations;
    this.code = getCodeFromList(validations);
  }
  
  public QRCodeException(List<Validation> validations, Throwable cause) {
    super(cause);
    this.validations = validations;
    this.code = getCodeFromList(validations);
  }
  
  public QRCodeException(List<Validation> validations, Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
    this.validations = validations;
    this.code = getCodeFromList(validations);
  }

  @Override
  public ErrorMessage getErrorMessage() {
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    
    if(org.apache.commons.lang3.StringUtils.isNotBlank(this.getMessage())) {
      dto.setMessage(this.getMessage());
    }
    
    if(this.getValidations() != null && !this.getValidations().isEmpty()) {
      dto.setValidations(this.getValidations());
    }
    
    dto.setTransactionId(this.getId());
    dto.setApplicationId(this.getApplicationId());
    dto.setApplicationName(this.getApplicationName());
    
    dto.setType(this.getType());
    
    return dto;
  }
  
  
  public List<Validation> getValidations() {
    return validations;
  }

  public void setValidations(List<Validation> validations) {
    this.validations = validations;
  }
  
  @Override
  public String getType() {
    return BUSINESS;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
  
  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  
  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
  
  @Override
  public String getMessage() {
    
    if(super.getMessage() == null || super.getMessage().equals("")) {
      return null;
    }
    
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("::");
    }
    if (id != null) {
      //builder.append(id);
      //builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }
  
  private String getCodeFromList(List<Validation> validations) {
    
    StringBuffer codes = new StringBuffer();
    if(validations != null) {
      for (Validation validation : validations) {
        codes.append(validation.getCode());
        codes.append(",");
      }
      
    }
    return codes.substring(0, codes.length()-1);
  }
  
  @Override
  public String toString() {
    return this.getClass().getName() + "["
            + "id="+getId()
            + "applicationId"+getApplicationId()
            + "applicationName="+getApplicationName()
            + "validations=" + validations 
            + "#\n#"
            + ", Class=" + getClass() 
            + ", Cause=" + getCause() 
            + ", Message=" + getMessage() 
            + ", LocalizedMessage=" + getLocalizedMessage()
            + "]";
  }*/
  
  
  
  
}
