package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.MeCard;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class ContactCodeInfo extends QRCode {

  private MeCard content;

  public ContactCodeInfo() {
    super(QRCodeType.CONTACT);
  }
  
  @Override
  public String generateString() {
    return this.content.generateString();
  }
  
}
