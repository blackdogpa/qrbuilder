package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.Url;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class URLCodeInfo extends QRCode {

  private Url content;
  
  public URLCodeInfo() {
    super(QRCodeType.URL);
    this.content = new Url();
  }
  
  public URLCodeInfo(String url) {
    this();
    setUrl(url);
  }
  
  
  @Override
  public String generateString() {
    return this.content.generateString();
  }
  
  
  public String getUrl() {
    return content.getUrl();
  }

  public URLCodeInfo setUrl(String url) {
    this.content.setUrl(url);
    return this;
  }

}
