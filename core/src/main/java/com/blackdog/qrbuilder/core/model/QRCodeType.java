package com.blackdog.qrbuilder.core.model;

public enum QRCodeType {

  TEXT, 
  CALL, 
  CONTACT, 
  EVENT, 
  GEO, 
  MAIL, 
  SMS, 
  URL, 
  VCARD, 
  WIFI, 
  EDOC;
}
