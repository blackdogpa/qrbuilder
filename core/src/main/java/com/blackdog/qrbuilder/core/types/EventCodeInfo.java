package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.IEvent;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class EventCodeInfo extends QRCode {

  /* TODO Correcao de problema com generateString() original  */
  public static final String LINE_FEED = "\n";
  public static final String NAME = "VEVENT";
  private static final String BEGIN_EVENT = "BEGIN:VEVENT";
  private static final String UID = "UID";
  private static final String STAMP = "DTSTAMP";
  private static final String ORGANIZER = "ORGANIZER";
  private static final String START = "DTSTART";
  private static final String END = "DTEND";
  private static final String SUMMARY = "SUMMARY";
  
  private IEvent content;
  
  public EventCodeInfo() {
    super(QRCodeType.EVENT);
    this.content = new IEvent();
  }
  
  @Override
  public String generateString() {
    //TODO Logica original com problema 
    //return this.content.generateString();
    
    StringBuilder sb = new StringBuilder();
    sb.append(BEGIN_EVENT).append(LINE_FEED);
    if (this.content.getUid() != null) {
      sb.append(UID).append(":").append(this.content.getUid()).append(LINE_FEED);
    } 
    if (this.content.getStamp() != null) {
      sb.append(STAMP).append(":").append(this.content.getStamp()).append(LINE_FEED);
    } 
    if (this.content.getOrganizer() != null) {
      sb.append(ORGANIZER).append(";").append(this.content.getOrganizer()).append(LINE_FEED);
    } 
    if (this.content.getStart() != null) {
      sb.append(START).append(":").append(this.content.getStart()).append(LINE_FEED);
    } 
    if (this.content.getEnd() != null) {
      sb.append(END).append(":").append(this.content.getEnd()).append(LINE_FEED);
    } 
    if (this.content.getSummary() != null) {
      sb.append(SUMMARY).append(":").append(this.content.getSummary()).append(LINE_FEED);
    }
    sb.append(LINE_FEED).append("END:VEVENT");
    return sb.toString();
    
  }
  
  
  public String getUid() {
    return this.content.getUid();
  }

  public EventCodeInfo setUid(String uid) {
    this.content.setUid(uid);
    return this;
  }

  public String getStamp() {
    return this.content.getStamp();
  }

  public EventCodeInfo setStamp(String stamp) {
    this.content.setStamp(stamp);
    return this;
  }

  public String getOrganizer() {
    return this.content.getOrganizer();
  }

  public EventCodeInfo setOrganizer(String organizer) {
    this.content.setOrganizer(organizer);
    return this;
  }

  public String getStart() {
    return this.content.getStart();
  }

  public EventCodeInfo setStart(String start) {
    this.content.setStart(start);
    return this;
  }

  public String getEnd() {
    return this.content.getEnd();
  }

  public EventCodeInfo setEnd(String end) {
    this.content.setEnd(end);
    return this;
  }

  public String getSummary() {
    return this.content.getSummary();
  }

  public EventCodeInfo setSummary(String summary) {
    this.content.setSummary(summary);
    return this;
  }

}
