package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class EDocumentCodeGenericInfo extends QRCode {

  private String content;
  
  public EDocumentCodeGenericInfo() {
    super(QRCodeType.EDOC);
  }
  
  @Override
  public String generateString() {
    return this.content;
  }
  
  public String getContent() {
    return content;
  }

  public EDocumentCodeGenericInfo setContent(String content) {
    this.content = content;
    return this;
  }

}
