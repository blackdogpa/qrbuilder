package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.Telephone;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class PhoneNumberCodeInfo extends QRCode {

  private Telephone content; //TODO Não encontrei a classe
  
  public PhoneNumberCodeInfo() {
    super(QRCodeType.CALL);
    this.content = new Telephone();
  }
  
  public PhoneNumberCodeInfo(String phoneNumber) {
    this();
    this.content.setTelephone(phoneNumber);
  }
  
  public String getPhoneNumber() {
    return content.getTelephone();
  }

  public PhoneNumberCodeInfo setPhoneNumber(String phoneNumber) {
    this.content.setTelephone(phoneNumber);
    return this;
  }

  @Override
  public String generateString() {
    return this.content.generateString();
  }

}
