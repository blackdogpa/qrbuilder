package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.Wifi;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class WifiCodeInfo extends QRCode {

  private Wifi content;
  
  public WifiCodeInfo() {
    super(QRCodeType.WIFI);
    this.content = new Wifi();
  }

  @Override
  public String generateString() {
    return this.content.generateString();
  }

  public String getAuthentication() {
    return this.content.getAuthentication();
  }

  public WifiCodeInfo setAuthentication(AuthenticationType authentication) {
    this.content.setAuthentication(authentication.toString());
    return this;
  }

  public String getSsid() {
    return  this.content.getSsid();
  }

  public WifiCodeInfo setSsid(String ssid) {
    this.content.setSsid(ssid);
    return this;
  }

  public String getPsk() {
    return this.content.getPsk();
  }

  public WifiCodeInfo setPsk(String psk) {
    this.content.setPsk(psk);
    return this;
  }

  public boolean isHidden() {
    return this.content.isHidden();
  }

  public WifiCodeInfo setHidden(final String value) {
    setHidden(Boolean.valueOf(value));
    return this;
  }

  public WifiCodeInfo setHidden(boolean hidden) {
    this.content.setHidden(hidden);
    return this;
  }

  public enum AuthenticationType {
    WEP, WPA, nopass;
  }
  
  
}
