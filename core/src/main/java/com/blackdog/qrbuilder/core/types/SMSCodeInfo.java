package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.SMS;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class SMSCodeInfo extends QRCode {

  private SMS content;
  
  public SMSCodeInfo() {
    super(QRCodeType.SMS);
    this.content = new SMS();
  }
  
  @Override
  public String generateString() {
    return this.content.generateString();
  }
  
  
  public String getNumber() {
    return this.content.getNumber();
  }

  public SMSCodeInfo setNumber(String number) {
    this.content.setNumber(number);
    return this;
  }

  public String getSubject() {
    return this.content.getSubject();
  }

  public SMSCodeInfo setSubject(String subject) {
    this.content.setSubject(subject);
    return this;
  }

}
