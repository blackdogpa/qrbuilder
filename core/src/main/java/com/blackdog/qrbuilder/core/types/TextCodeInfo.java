package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class TextCodeInfo extends QRCode {

  private String content; //TODO Tipo Não encontrado
  
	public TextCodeInfo() {
    super(QRCodeType.TEXT);
	}

  public TextCodeInfo(String content) {
    super(QRCodeType.TEXT);
    this.content = content;
  }

  @Override
  public String generateString() {
    return this.content;
  }

}
