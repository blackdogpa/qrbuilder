package com.blackdog.qrbuilder.core.model.cryptografy;

public class RSAConfig extends CryptoConfig {

	private String privateKey;
	private String publicKey;
	
	public RSAConfig() {
		super();
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public RSAConfig setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
		return this;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public RSAConfig setPublicKey(String publicKey) {
		this.publicKey = publicKey;
		return this;
	}
	
	
	
	
}
