package com.blackdog.qrbuilder.core.model;

public enum ErrorLevel {

	L("L", com.google.zxing.qrcode.decoder.ErrorCorrectionLevel.L),
	M("M", com.google.zxing.qrcode.decoder.ErrorCorrectionLevel.M),
	Q("Q", com.google.zxing.qrcode.decoder.ErrorCorrectionLevel.Q),
	H("H", com.google.zxing.qrcode.decoder.ErrorCorrectionLevel.H);
	
	private String id;
	private com.google.zxing.qrcode.decoder.ErrorCorrectionLevel level;
	
	private ErrorLevel(String id, com.google.zxing.qrcode.decoder.ErrorCorrectionLevel level) {
		this.id = id;
		this.level = level;
	}
	
	public String getId() {
		return id;
	}

	public com.google.zxing.qrcode.decoder.ErrorCorrectionLevel getLevel() {
		return level;
	}
	
	
}
