package com.blackdog.qrbuilder.core.types;

import java.util.List;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

import net.glxn.qrgen.core.scheme.GeoInfo;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class GeoLocationCodeInfo extends QRCode {

  private GeoInfo content;
  
  public GeoLocationCodeInfo() {
    super(QRCodeType.GEO);
    this.content = new GeoInfo();
  }
  
  public List<String> getPoints() {
    return this.content.getPoints();
  }

  public GeoLocationCodeInfo setPoints(List<String> points) {
    this.content.setPoints(points);
    return this;
  }
  
  @Override
  public String generateString() {
    return this.content.generateString();
  }
}
