package com.blackdog.qrbuilder.core.model;

import com.blackdog.qrbuilder.core.exceptions.QRCodeException;
import com.blackdog.qrbuilder.core.model.cryptografy.CryptoConfig;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public class QRCodeConfig {

  /** Config **/

  private Integer size = 1024;
  private OutputFormat outputFormat;
  private CryptoConfig cryptoConfig;

  private ErrorLevel errorLevel;
  private ImageTypes imageType;

  /** Content **/
  //protected Schema content;

  public QRCodeConfig() {
    super();
  }

  public void specificValidations() throws QRCodeException {

    if(this.getSize() == null) {
      this.setSize(1024);
    }
    
    if(this.getOutputFormat() == null) {
      this.setOutputFormat(OutputFormat.JPG);
    }
    
    if(this.getImageType() == null) {
      this.setImageType(ImageTypes.JPG);
    }
    
  }
  
  /**
   * 
   * @param size Size in pixels
   * 
   * @return
   */
  public QRCodeConfig setSize(Integer size) {
    this.size = size;
    return this;
  }

  public Integer getSize() {
    return size;
  }

  public OutputFormat getOutputFormat() {
    return outputFormat;
  }

  /**
   * 
   * @param outputFormat JPG, GIF or PNG
   * @return
   */
  public QRCodeConfig setOutputFormat(OutputFormat outputFormat) {
    this.outputFormat = outputFormat;

    return this;
  }

  public CryptoConfig getCryptoConfig() {
    return cryptoConfig;
  }

  public QRCodeConfig setCryptoConfig(CryptoConfig cryptoConfig) {
    this.cryptoConfig = cryptoConfig;

    return this;
  }

  public ErrorLevel getErrorLevel() {
    return errorLevel;
  }

  public QRCodeConfig setErrorLevel(ErrorLevel errorLevel) {
    this.errorLevel = errorLevel;
    return this;
  }

  public ImageTypes getImageType() {
    return imageType;
  }

  public QRCodeConfig setImageType(ImageTypes imageType) {
    this.imageType = imageType;
    return this;
  }

}
