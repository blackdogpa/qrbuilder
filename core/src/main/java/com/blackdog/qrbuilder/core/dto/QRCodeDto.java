package com.blackdog.qrbuilder.core.dto;

import com.blackdog.qrbuilder.core.model.OutputFormat;
import com.blackdog.qrbuilder.core.model.QRCodeType;
import com.blackdog.qrbuilder.core.model.cryptografy.CryptoConfig;

public class QRCodeDto {
  
  
private QRCodeType type;
  
  /** Config **/
  
  private Integer size = 1024;
  private OutputFormat outputFormat = OutputFormat.JPG;
  private CryptoConfig cryptoConfig;
  
  /** Content **/
  private String codeImage;
  private String debug;

  public QRCodeType getType() {
    return type;
  }

  public void setType(QRCodeType type) {
    this.type = type;
  }

  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public OutputFormat getOutputFormat() {
    return outputFormat;
  }

  public void setOutputFormat(OutputFormat outputFormat) {
    this.outputFormat = outputFormat;
  }

  public CryptoConfig getCryptoConfig() {
    return cryptoConfig;
  }

  public void setCryptoConfig(CryptoConfig cryptoConfig) {
    this.cryptoConfig = cryptoConfig;
  }

  public String getCodeImage() {
    return codeImage;
  }

  public void setCodeImage(String codeImage) {
    this.codeImage = codeImage;
  }

  public String getDebug() {
    return debug;
  }

  public void setDebug(String debug) {
    this.debug = debug;
  }

  @Override
  public String toString() {
    return "QRCodeDto [type=" + type + ", size=" + size + ", outputFormat=" + outputFormat + ", cryptoConfig=" 
                      + cryptoConfig + ", debug=" + debug + ",\n codeImage=" + codeImage + "]";
  }
  

}
