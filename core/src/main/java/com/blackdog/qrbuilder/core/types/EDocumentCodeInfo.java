package com.blackdog.qrbuilder.core.types;

import com.blackdog.qrbuilder.core.model.QRCode;
import com.blackdog.qrbuilder.core.model.QRCodeType;

/**
 * TODO Não implementado ainda
 *   
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public class EDocumentCodeInfo extends QRCode {

  private String image;
  private String content;
  
  public EDocumentCodeInfo() {
    super(QRCodeType.EDOC);
  }
  
  @Override
  public String generateString() {
    return this.content;
  }
  
  public String getImage() {
    return image;
  }

  public EDocumentCodeInfo setImage(String image) {
    this.image = image;
    return this;
  }
  
  

}
