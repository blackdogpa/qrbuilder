package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.URLCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class URLBuilderTest extends QRBuilderTest {


	@Test
	public void testMin() throws Exception {
	  builder = new QRConfigBuilder();
    
    code = builder.setCode(new URLCodeInfo("http://google.com")).build();
    
    System.out.println(code);
    System.out.println(code.getDebug());
    
    assertEquals("http://google.com".toUpperCase(), code.getDebug());
    
    save("/home/thiago/Downloads/qrUrl.jpg", code.getCodeImage());
	}
	
	@Test
  public void testMin2() throws Exception {
    builder = new QRConfigBuilder();
    
    code = builder.setCode(new URLCodeInfo().setUrl("http://google.com")).build();
    
    System.out.println(code);
    System.out.println(code.getDebug());
    
    assertEquals("http://google.com".toUpperCase(), code.getDebug());
    
    save("/home/thiago/Downloads/qrUrl2.jpg", code.getCodeImage());
  }


}
