package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.GeoLocationCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class GeoLocationBuilderTest extends QRBuilderTest {

  
  @Test
  public void testMin() throws Exception {
    builder = new QRConfigBuilder();
    
    List<String> points = new ArrayList<String>();
    points.add("001");
    points.add("002");
    
    GeoLocationCodeInfo card = new GeoLocationCodeInfo().setPoints(points);

    code = new QRConfigBuilder().setCode(card).build();
    
    System.out.println(code.getDebug());
    
    assertTrue(code.getDebug().equals("geo:001,002"));
    
    save("/home/thiago/Downloads/qrEDocGeneric.jpg", code.getCodeImage());
  }
  
  


}
