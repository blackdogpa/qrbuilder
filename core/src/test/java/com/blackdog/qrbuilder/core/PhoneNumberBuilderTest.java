package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.PhoneNumberCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class PhoneNumberBuilderTest extends QRBuilderTest {

	@Test
	public void testMin() throws Exception {
	  builder = new QRConfigBuilder();
    
    code = builder.setCode(new PhoneNumberCodeInfo("(91) 99999-9999")).build();
    
    System.out.println(code);
    //System.out.println(code.getDebug());
    
    assertEquals("tel:(91) 99999-9999", code.getDebug());
    
    save("/home/thiago/Downloads/qrPhone.jpg", code.getCodeImage());
	}

	@Test
  public void testMin2() throws Exception {
    builder = new QRConfigBuilder();
    
    code = builder.setCode(new PhoneNumberCodeInfo().setPhoneNumber("(91) 99999-9999")).build();
    
    System.out.println(code);
    //System.out.println(code.getDebug());
    
    assertEquals("tel:(91) 99999-9999", code.getDebug());
    
    save("/home/thiago/Downloads/qrPhone.jpg", code.getCodeImage());
  }
	
}
