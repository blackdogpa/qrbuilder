package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.EDocumentCodeGenericInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class EDocumentBuilderTest extends QRBuilderTest {

  
  @Test
  public void testNewTextCodeMin() throws Exception {
    builder = new QRConfigBuilder();
    
    EDocumentCodeGenericInfo card = new EDocumentCodeGenericInfo().setContent("CODECONTENT");

    code = new QRConfigBuilder().setCode(card).build();
    
    System.out.println(code.getDebug());
    
    assertTrue(code.getDebug().equals("CODECONTENT"));
    
    assertEquals("CODECONTENT", code.getDebug());
    
    save("/home/thiago/Downloads/qrEDocGeneric.jpg", code.getCodeImage());
  }
  
  


}
