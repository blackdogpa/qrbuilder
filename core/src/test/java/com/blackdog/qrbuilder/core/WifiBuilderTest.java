package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.WifiCodeInfo;
import com.blackdog.qrbuilder.core.types.WifiCodeInfo.AuthenticationType;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class WifiBuilderTest extends QRBuilderTest {

  
  @Test
  public void testMin() throws Exception {
    builder = new QRConfigBuilder();
    
    WifiCodeInfo card = new WifiCodeInfo()
                          .setAuthentication(AuthenticationType.WPA)
                          .setSsid("TESTEWIFI")
                          .setHidden(true)
                          .setPsk("MY_PSK");

    code = new QRConfigBuilder().setCode(card).build();
    
    System.out.println(code.getDebug());
    
    assertEquals("WIFI:S:TESTEWIFI;T:WPA;P:MY_PSK;H:true;", code.getDebug());
    
    save("/home/thiago/Downloads/qrEDocGeneric.jpg", code.getCodeImage());
  }
  
  


}
