package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.SMSCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class SMSBuilderTest extends QRBuilderTest {

  
  @Test
  public void testMin() throws Exception {
    builder = new QRConfigBuilder();
    
    SMSCodeInfo card = new SMSCodeInfo().setNumber("3333-44444").setSubject("Teste Subject");

    code = new QRConfigBuilder().setCode(card).build();
    
    System.out.println(code.getDebug());
    
    assertEquals("sms:3333-44444:Teste Subject", code.getDebug());
    
    save("/home/thiago/Downloads/qrEDocGeneric.jpg", code.getCodeImage());
  }
  
  


}
