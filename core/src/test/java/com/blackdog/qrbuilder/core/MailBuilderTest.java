package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.MailCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class MailBuilderTest extends QRBuilderTest {

  
  @Test
  public void testMin() throws Exception {
    builder = new QRConfigBuilder();
    
    code = builder.setCode(new MailCodeInfo("test@gmail.com")).build();
    
    System.out.println(code);

    assertEquals("mailto:test@gmail.com", code.getDebug());
    
    save("/home/thiago/Downloads/qrEMail1.jpg", code.getCodeImage());
  }
  
  
  @Test
  public void testMin2() throws Exception {
    
    
    builder = new QRConfigBuilder();
    
    code = builder.setCode(new MailCodeInfo().setEmail("test@gmail.com")).build();
    
    System.out.println(code);

    assertEquals("mailto:test@gmail.com", code.getDebug());
    
    save("/home/thiago/Downloads/qrEMail1.jpg", code.getCodeImage());
    
    
  }
  
  


}
