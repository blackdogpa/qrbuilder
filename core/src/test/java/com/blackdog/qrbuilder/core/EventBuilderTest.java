package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.EventCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class EventBuilderTest extends QRBuilderTest {

  
  @Test
  public void testNewTextCodeMin() throws Exception {
    builder = new QRConfigBuilder();
    
    EventCodeInfo card = new EventCodeInfo()
                              .setUid("UID001")
                              .setStamp("STAMPPP")
                              .setOrganizer("ORG")
                              .setStart("STAR")
                              .setEnd("ENDDD")
                              .setSummary("SUMMM");

    code = new QRConfigBuilder().setCode(card).build();
    
    System.out.println(code.getDebug());
    
    assertTrue(code.getDebug().startsWith("BEGIN:VEVENT"));
    assertTrue(code.getDebug().contains("UID:UID001"));
    
    save("/home/thiago/Downloads/qrEvent.jpg", code.getCodeImage());
  }
  
  


}
