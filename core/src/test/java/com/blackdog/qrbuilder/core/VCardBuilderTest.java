package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.blackdog.qrbuilder.core.types.VCardCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class VCardBuilderTest extends QRBuilderTest {

	@Test
	public void testMin() throws Exception {
		
	  VCardCodeInfo card = new VCardCodeInfo()
	                             .setName("My NOME")
	                             .setPhoneNumber("(91) 3333-3333")
	                             ;
	  
	  code = new QRConfigBuilder()
	             .setCode(card)
	             .build();
	  
	  System.out.println(code.getDebug());
    
	  
	  assertTrue(code.getDebug().startsWith("BEGIN:VCARD"));
    
	  assertTrue(code.getDebug().contains("N:My NOME"));
	  assertTrue(code.getDebug().contains("TEL:(91) 3333-3333"));
	  
	  //assertEquals("tel:(91) 99999-9999", code.get);
    
    save("/home/thiago/Downloads/qrVCard1.jpg", code.getCodeImage());
	  
	  
	}
	
	@Test
  public void testMin2() throws Exception {
    
    VCardCodeInfo card = new VCardCodeInfo()
                               .setName("My NOME")
                               .setCompany("My Company")
                               .setTitle("Some title")
                               .setPhoneNumber("(91) 3333-3333")
                               .setEmail("")
                               .setAddress("Aqui mesmo")
                               .setWebsite("")
                               .setNote("Some Notes")
                               ;
    
    code = new QRConfigBuilder()
            .setCode(card)
            .build();
 
   System.out.println(code.getDebug());
   
   assertTrue(code.getDebug().startsWith("BEGIN:VCARD"));
   
   assertTrue(code.getDebug().contains("N:My NOME"));
   assertTrue(code.getDebug().contains("TEL:(91) 3333-3333"));
   
   //assertEquals("tel:(91) 99999-9999", code.get);
   
   save("/home/thiago/Downloads/qrVCard2.jpg", code.getCodeImage());
    
    
  }

	
}
