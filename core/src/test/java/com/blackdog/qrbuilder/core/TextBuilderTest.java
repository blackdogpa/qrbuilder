package com.blackdog.qrbuilder.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.blackdog.qrbuilder.core.exceptions.QRCodeException;
import com.blackdog.qrbuilder.core.model.cryptografy.RSAConfig;
import com.blackdog.qrbuilder.core.types.TextCodeInfo;
import com.blackdog.qrbuilder.core.util.QRBuilderTest;

public class TextBuilderTest extends QRBuilderTest {

  
  @Test
  public void testMin() throws Exception {
    builder = new QRConfigBuilder();
    
    code = builder.setCode(new TextCodeInfo("Test Text Code"))
                  .build();
    
    System.out.println(code);
    System.out.println();
    System.out.println(code.getDebug());
    
    assertEquals("Test Text Code", code.getDebug());
    
    save("/home/thiago/Downloads/qrText.jpg", code.getCodeImage());
  }
  
  
	@Test
	public void testNewTextCode() throws QRCodeException{
	  builder = new QRConfigBuilder();
	  
	  /*String qrString =

	          builder.newTextCode()
	                 .setSize(720)
	                 .setOutputFormat(OutputFormat.JPG)
	                 .buildAsBase64();
	  
		System.out.println(qrString);*/
	}
	
	
	@Test
  public void testNewTextCodeGZIP() throws QRCodeException {
    
    RSAConfig RSAConfig = new RSAConfig().setPrivateKey("").setPublicKey("");
    
    /*String qrString = 
    builder.newURLCode()
         .setSize(720)
         .setOutputFormat(OutputFormat.JPG)
         .setCryptoConfig(new RSAConfig()
                    .setPrivateKey("")
                    .setPublicKey("")
                  )
         .buildAsBase64();
    
    System.out.println(qrString);*/
  }
	
	@Test
  public void testNewTextCodeEncript() throws QRCodeException {
    
    RSAConfig RSAConfig = new RSAConfig().setPrivateKey("").setPublicKey("");
    
    /*String qrString = 
    builder.newURLCode()
         .setSize(720)
         .setOutputFormat(OutputFormat.JPG)
         .setCryptoConfig(new RSAConfig()
                    .setPrivateKey("")
                    .setPublicKey("")
                  )
         .buildAsBase64();
    
    System.out.println(qrString);*/
  }


}
