package com.blackdog.qrbuilder.core.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import org.junit.Before;

import com.blackdog.qrbuilder.core.QRConfigBuilder;
import com.blackdog.qrbuilder.core.dto.QRCodeDto;
import com.blackdog.qrbuilder.core.exceptions.QRCodeException;

public abstract class QRBuilderTest {

	protected QRConfigBuilder builder;
	protected QRCodeDto code;
	
	@Before
	public void init() throws QRCodeException {
		/*QRCodeDto codeDto = new QRConfigBuilder()
		                        .setCode(new VCardCodeInfo().setCompany("nnnnnn")
		                                )
		                        .build();*/
		
	}
	
	
	public void save(String path, String content) throws Exception {
	  
	  
	  Path destinationFile = Paths.get(path);
	  Files.write(destinationFile, Base64.getDecoder().decode(content));
	  
	}


}
